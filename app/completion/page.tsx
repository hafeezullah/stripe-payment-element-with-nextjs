'use client'
import {useEffect, useState} from 'react';
import { loadStripe } from '@stripe/stripe-js';
import { getPublishableKey } from '../api/create-payment-intent/keys';

function Completion() {
 
  return (
    <>
      <h1>Thank you!</h1>
      <a href="/">home</a>
      <div id="messages" role="alert"></div>
    </>
  );
}

export default Completion;