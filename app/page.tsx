'use client'
import React, { useEffect, useState } from 'react';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import CheckoutForm from './components/checkout-form';
import { getStripePublishableKey } from './api/create-payment-intent/keys';

const publishableKey =  getStripePublishableKey();
const stripePromise = loadStripe(publishableKey);

export default function Home() {
  
  const [clientSecret, setClientSecret] = useState(null);
  
  useEffect(() => {
   
    // Fetch the client secret from your backend
    const fetchClientSecret = async () => {
      const response = await fetch('/api/create-payment-intent', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ amount: 1000, currency: 'usd' }), // Adjust as needed
      });

      const data = await response.json();
      setClientSecret(data.clientSecret);
    };

    fetchClientSecret();
  }, []);

  if (!clientSecret) {
    return <div>Loading payment information...</div>;
  }
  
  return (
    <>
      <Elements stripe={stripePromise} options={{ clientSecret,externalPaymentMethodTypes: ['external_bank_pay','external_paypay','external_gcash'] }}>
        <CheckoutForm />
      </Elements>
  </>
  );
}
