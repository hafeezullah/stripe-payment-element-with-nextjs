'use client'
import {useState} from 'react'
import {PaymentElement,useStripe, useElements} from '@stripe/react-stripe-js'

  export default function CheckoutForm() {
    
    const stripe = useStripe();
    const elements = useElements();
    const [message, setMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);
  
    const handleSubmit = async (e:any) => {
      e.preventDefault();
      if (!stripe || !elements) {
        return;
      }
      setIsLoading(true);
      const { error } = await stripe.confirmPayment({
        elements,
        confirmParams: {
          return_url: `${window.location.origin}/completion`,
        },
      });

      if (error.type === "card_error" || error.type === "validation_error") {
        setMessage(error.message ?? "");
      } else {
        setMessage("An unexpected error occured.");
      }
  
      setIsLoading(false);
    }
    
    return (
      <form id="payment-form" onSubmit={handleSubmit}>
        <h1>Nextjs Stripe Payment Element</h1>
        <PaymentElement id="payment-element" />
        <button disabled={isLoading || !stripe || !elements} id="submit">
          <span id="button-text">
            {isLoading ? <div className="spinner" id="spinner"></div> : "Pay now"}
          </span>
        </button>
        {message && <div id="payment-message">{message}</div>}
      </form>
    )
  }